#!/bin/bash

TAG=$1
EMAIL=chaica@ohmytux.com
SUBJECT="Script launch-brebis-tests returned an error"
WORKSPACE=/home/chaica
TESTDIR=$WORKSPACE/archives-for-brebis-tests
LOG=/var/log/brebis-functional-tests.log

# This script performs basic functional tests of
# large archives (type tar.gz and tar.bz2)
# The admin is warned by email

perform_test(){
	FILEPATH=$1
	# launch test
	$TESTDIR/../brebis/brebis.py -G  $FILEPATH
	if [[ $? != 0 ]]; then
		echo "`date +'%H:%M:%S - %d-%m-%Y'` - error while parsing the archive $FILEPATH" >> $LOG
		echo "`date +'%H:%M:%S - %d-%m-%Y'` - testing the next archive" >> $LOG
		echo "error while parsing the archive $FILEPATH"|mail -s "${SUBJECT}" $EMAIL
		return
	fi
	PATHBASE=`dirname $FILEPATH`
	RES=`find $PATHBASE -type f \( -iname "*conf" -o -iname "*list" \)|wc -l`
	if [[ $RES != 2 ]]; then
		echo "`date +'%H:%M:%S - %d-%m-%Y'` - error while checking the number of files of $FILEPATH" >> $LOG
		echo "`date +'%H:%M:%S - %d-%m-%Y'` - testing the next archive" >> $LOG
		echo "error while checking the number of files of $FILEPATH"|mail -s "${SUBJECT}" $EMAIL
		return
	fi
	echo "`date +'%H:%M:%S - %d-%m-%Y'` - leaving tests of the archive $FILEPATH">> $LOG
}

# start the sessions
echo "`date +'%H:%M:%S - %d-%m-%Y'` - starting session of functional tests for brebis" >> $LOG
# checkout the last version of brebis of the given tag
rm -rf $WORKSPACE/brebis
echo "`date +'%H:%M:%S - %d-%m-%Y'` - checking out the brebis repository" >> $LOG
cd $WORKSPACE
hg clone  http://hg.brebisproject.org/brebis
if [[ $? != 0 ]]; then
	echo "`date +'%H:%M:%S - %d-%m-%Y'` - error while checking out the brebis repository" >> $LOG
	echo "error while checking out the brebis repository"|mail -s "${SUBJECT}" $EMAIL
	exit 1
fi
cd $WORKSPACE/brebis
if [ "$TAG" == "" ]; then
	hg update default
else
	hg update $TAG
	if [[ $? != 0 ]]; then
		echo "`date +'%H:%M:%S - %d-%m-%Y'` - error while updating to the $TAG branch" >> $LOG
		echo "error while updating to the $TAG branch"|mail -s "${SUBJECT}" $EMAIL
		exit 1
	fi
fi
sed -i 's/python3.3/python3.4/g' $WORKSPACE/brebis/brebis.py
# remove the useless previously generated files
find $TESTDIR -type f \( -iname "*conf" -o -iname "*list" \) -exec rm {} \;
if [[ $? != 0 ]]; then
	echo "`date +'%H:%M:%S - %d-%m-%Y'` - error while removing old conf and list files of $FILEPATH" >> $LOG
	echo "error while removing old conf and list files of $FILEPATH"|mail -s "${SUBJECT}" $EMAIL
	exit 1
fi

ARCHIVES="backup-sys-tar-bz2-578-MB/backup-sys-tar-bz2-578-MB.tar.bz2 \
backup-sys-tar-gz-611-MB/backup-sys-tar-gz-611-MB.tar.gz \
backup-sys-tar-bz2-2-GB/backup-sys-tar-bz2-2-GB.tar.bz2 \
backup-sys-tar-bz2-3-4-GB/backup-sys-tar-bz2-3-4-GB.tar.bz2 \
backup-sys-tar-gz-3-6-GB/backup-sys-tar-gz-3-6-GB.tar.gz"
for i in $ARCHIVES
do
	echo "`date +'%H:%M:%S - %d-%m-%Y'` - now testing $i" >> $LOG
	perform_test $TESTDIR/$i
done
echo "`date +'%H:%M:%S - %d-%m-%Y'` - leaving session of functional tests for brebis" >> $LOG
exit 0
